package uvsq21304441.RPN;
import java.util.Scanner; 

public class SaisieRPN {
	/**
	 * Classe permettant l'interaction avec l'utilisateur; le programme s'arrete en entrant "exit"
	 **/
	public void saisie()
	{
		char ope;
		MoteurRPN r = new MoteurRPN();

		System.out.println("Calculatrice RPN, entrer 'exit' pour quitter \n\n");
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Ajoutez une operande ou entrez une operation : ");
		String input = sc.nextLine();
		
			while(!input.equals("exit"))
			{	  
				try
				{
					ope = input.charAt(0);
					if(ope=='+')
					{
						r.ope(Operation.PLUS);
						System.out.println(r.toString());
					}else if(ope=='-')
					{
						r.ope(Operation.MOINS);
						System.out.println(r.toString());
					}else if(ope=='/')
					{
						r.ope(Operation.DIV);
						System.out.println(r.toString());
					}else if(ope=='*')
					{
						r.ope(Operation.MULT);
						System.out.println(r.toString());
					}else
					{
						double operande=Double.parseDouble(input);
						r.ajout(operande);
						System.out.println(r.toString());
					}
				}
				catch(NumberFormatException e) //si l'entr�e n'est pas un double,+,-,* ou /
					{
						System.out.println("Entree non valide (1) \n");
					}
					
				catch(StringIndexOutOfBoundsException e) //si aucune entr�e
					{
						System.out.println("Entree non valide (2) \n");
					}
					
				System.out.println("Ajoutez une operande ou entrez une operation : ");
				input=sc.nextLine();
			}
			System.exit(0);
	}
}


