package uvsq21304441.RPN;
import java.util.Vector;

public class MoteurRPN {
	private Vector<Double>operande;
	private static int MIN = Math.abs(0);
	private static int MAX = Math.abs(1000);
	
	public MoteurRPN(){
		this.operande = new Vector<Double>();
	}
	
	public boolean ajout(double a){	
		try{
		if(a<MIN || a>MAX)
		{
			throw new ValeurNonAdmiseException();
		}else{
				this.operande.add(a);
				return true;
			}
		}
		catch(ValeurNonAdmiseException e){	
			return false;
		}
	}
	
	public boolean ope(Operation o){
			if(operande.size()>=2){
				int iterator=operande.size();
				double b =operande.remove(iterator-1);
				double a = operande.remove(iterator-2);
				
				//ajout du resultat de l'op�ration
				if(!this.ajout(o.eval(a,b))){
					this.ajout(a);
					this.ajout(b);
					return false;
				}
				return true;
			}
			else{
				System.out.println("Pas assez d'operandes ! \n");
				return false;
			}
	}
	
	public String toString()
	{
		String aff="";
		if(!operande.isEmpty())
		{
			int iterator;
			aff+="Operande restantes : [";
			for(iterator=0;iterator<operande.size();iterator++)
			{
				if(iterator!=operande.size()-1)
					aff+=operande.elementAt(iterator)+" ; ";
				else
					aff+=operande.elementAt(iterator)+"]\n\n";
			}
		}else{return "Aucune operande restante \n\n";}
		return aff;
	}
}
