package uvsq21304441.RPN;

public enum Operation {
	PLUS('+'){
		@Override
		public double eval(double a,double b)
		{
			return a+b;
		}
	},
	MOINS('-'){
		@Override
		public double eval(double a,double b)
		{
			return a-b;
		}
	},
	DIV('/'){
		@Override
		public double eval(double a,double b)
		{
				return a/b;
		}
	},
	MULT('*'){
		@Override
		public double eval(double a,double b)
		{
			return a*b;
		}
	};
	private final char symbole;
	public abstract double eval(double a , double b);
	private Operation(char symbole){
		this.symbole=symbole;
	}
}
