package uvsq21304441.RPN;
import static org.junit.Assert.*;
import org.junit.Test;


public class MoteurRPNTest {
	@Test
	public void testAjout() {
		MoteurRPN r = new MoteurRPN();
		assertTrue(r.ajout(4.5));
	}
	@Test
	public void testAjout2() {
		MoteurRPN r = new MoteurRPN();
		assertFalse(r.ajout(-9));
	}
	@Test
	public void testAjout3() {
		MoteurRPN r = new MoteurRPN();
		assertFalse(r.ajout(1001));
	}
	@Test
	public void saisieOperationDivParZero() {
		MoteurRPN r=new MoteurRPN();
		r.ajout(4.0);
		r.ajout(0.0);
		assertFalse(r.ope(Operation.DIV));
	}
	@Test
	public void saisieOperationResNegSoustraction() {
		MoteurRPN r=new MoteurRPN();
		r.ajout(0.0);
		r.ajout(4.0);
		assertFalse(r.ope(Operation.MOINS));
	}
	
	
}
