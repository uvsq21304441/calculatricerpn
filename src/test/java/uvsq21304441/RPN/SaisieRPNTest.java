package uvsq21304441.RPN;

import static org.junit.Assert.*;

import org.junit.Test;

public class SaisieRPNTest {

	@Test
	public void saisieOperationPlus() {
		MoteurRPN r=new MoteurRPN();
		r.ajout(4.0);
		r.ajout(4.0);
		assertTrue(r.ope(Operation.PLUS));
		assertFalse(r.ope(Operation.PLUS));
	}
	@Test
	public void saisieOperationMoins() {
		MoteurRPN r=new MoteurRPN();
		r.ajout(4.0);
		r.ajout(4.0);
		assertTrue(r.ope(Operation.MOINS));
		assertFalse(r.ope(Operation.MOINS));
	}
	@Test
	public void saisieOperationMult() {
		MoteurRPN r=new MoteurRPN();
		r.ajout(4.0);
		r.ajout(4.0);
		assertTrue(r.ope(Operation.MULT));
		assertFalse(r.ope(Operation.MULT));
	}
	@Test
	public void saisieOperationDiv() {
		MoteurRPN r=new MoteurRPN();
		r.ajout(4.0);
		r.ajout(4.0);
		assertTrue(r.ope(Operation.DIV));
		assertFalse(r.ope(Operation.DIV));
	}

}
